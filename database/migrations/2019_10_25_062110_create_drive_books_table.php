<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriveBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drive_books', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['private', 'business'])->default('private')->nullable(false);
            $table->dateTime('time_from')->nullable(false);
            $table->dateTime('time_to')->nullable(false);
            $table->string('place_from')->nullable(false);
            $table->string('place_to')->nullable(false);
            $table->integer('distance')->nullable(false);
            $table->string('purpose_drive')->nullable();
            $table->integer('mileage')->nullable();
            $table->enum('refueling', ['cash', 'card'])->default('card')->nullable();

            $table->unsignedInteger('user_id')->nullable(false);
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drive_books');
    }
}
