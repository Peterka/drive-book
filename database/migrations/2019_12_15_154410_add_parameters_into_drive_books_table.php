<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParametersIntoDriveBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drive_books', function (Blueprint $table) {
            $table->dropColumn(['mileage']);
            $table->string('through')->nullable();
            $table->integer('tach_before')->nullable();
            $table->integer('tach_after')->nullable();
            $table->boolean('back')->nullable(false)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drive_books', function (Blueprint $table) {
            $table->integer('mileage')->nullable();
            $table->dropColumn(['through', 'tach_before', 'tach_after', 'back']);
        });
    }
}
