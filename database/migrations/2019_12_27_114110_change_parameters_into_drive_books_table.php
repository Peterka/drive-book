<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeParametersIntoDriveBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drive_books', function (Blueprint $table) {
            $table->dropColumn(['time_from', 'time_to', 'distance', 'refueling']);
            $table->dateTime('date')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drive_books', function (Blueprint $table) {
            $table->dateTime('time_from')->nullable(false);
            $table->dateTime('time_to')->nullable(false);
            $table->integer('distance')->nullable(false);
            $table->enum('refueling', ['cash', 'card'])->default('card')->nullable();
        });
    }
}
