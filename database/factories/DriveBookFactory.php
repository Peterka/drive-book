<?php

/** @var Factory $factory */

use App\Models\DriveBook;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(DriveBook::class, function (Faker $faker) {
    return [
        'type' => 'private',
        'date' => $faker->dateTimeBetween('-6 months', '+6 months'),
        'place_from' => $faker->name,
        'place_to' => $faker->name,
        'through' => $faker->name,
        'back' => $faker->boolean,
        'purpose_drive' => $faker->words(3, true),
        'tach_before' => $faker->randomDigitNotNull,
        'tach_after' => $faker->randomDigitNotNull,
        'user_id' => optional(User::query()->first())->id ?? (factory(User::class)->create())->id
    ];
});
