<?php

namespace App\Exports;

use App\Models\DriveBook;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class DriveBookExport implements FromCollection
{
    /**
    * @return Collection
    */
    public function collection(): Collection
    {
        /** @var Collection $driveBooks */
        $driveBooks = DriveBook::query()
            ->select(['type', 'date', 'place_from', 'place_to', 'through', 'back', 'purpose_drive', 'tach_before',
                'tach_after'])->get();
        /** @var array $header */
        $header = ['type', 'date', 'place_from', 'place_to', 'through', 'back', 'purpose_drive', 'tach_before',
            'tach_after'];
        $driveBooks->prepend($header);
        return $driveBooks;
    }
}
