<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\DriveBook;
use App\Http\Requests\DriveBookStoreRequest;
use App\Http\Requests\DriveBookUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DriveBookService
{
    /**
     * @param DriveBookStoreRequest $request
     * @return DriveBook
     */
    public function create(DriveBookStoreRequest $request): DriveBook
    {
        /** @var DriveBook $driveBook */
        $driveBook = new DriveBook();
        DB::beginTransaction();
        $this->drive($driveBook, $request);
        $driveBook->user_id = $request->user('api')->id;
        $driveBook->save();
        DB::commit();
        return $driveBook;
    }

    /**
     * @param DriveBookUpdateRequest $request
     * @param DriveBook $driveBook
     * @return DriveBook
     */
    public function update(DriveBookUpdateRequest $request, DriveBook $driveBook): DriveBook
    {
        DB::beginTransaction();
        $this->drive($driveBook, $request);
        $driveBook->save();
        DB::commit();
        return $driveBook;
    }

    /**
     * @param DriveBook $driveBook
     * @param Request $request
     * @return DriveBook
     */
    public function drive(DriveBook $driveBook, Request $request): DriveBook
    {
        $driveBook->type = $request->input('type');
        $driveBook->date = $request->input('date');
        $driveBook->place_from = $request->input('place_from');
        $driveBook->place_to = $request->input('place_to');
        $driveBook->through = $request->input('through');
        $driveBook->back = $request->input('back');
        $driveBook->purpose_drive = $request->input('purpose_drive');
        $driveBook->tach_before = $request->input('tach_before');
        $driveBook->tach_after = $request->input('tach_after');
        return $driveBook;
    }
}
