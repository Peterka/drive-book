<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DriveBookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'date'	=> $this->date->format('c'),
            'place_from' => $this->place_from,
            'place_to' => $this->place_to,
            'through' => $this->through,
            'back' => $this->back,
            'purpose_drive' => $this->purpose_drive,
            'tach_before' => $this->tach_before,
            'tach_after' => $this->tach_after,
            'user_id' => $this->user_id,
        ];
    }
}
