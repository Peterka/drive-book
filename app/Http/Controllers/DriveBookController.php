<?php

namespace App\Http\Controllers;

use App\Exceptions\CannotDeleteDriveException;
use App\Exceptions\CannotUpdateDriveException;
use App\Models\DriveBook;
use App\Http\Requests\DriveBookDestroyRequest;
use App\Http\Requests\DriveBookStoreRequest;
use App\Http\Requests\DriveBookUpdateRequest;
use App\Http\Resources\DriveBookResource;
use App\Services\DriveBookService;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class DriveBookController extends Controller
{
    /**
     * @var DriveBookService
     */
    private $driveBookService;

    /**
     * DriveBookController constructor.
     * @param DriveBookService $driveBookService
     */
    public function __construct(DriveBookService $driveBookService)
    {
        $this->driveBookService = $driveBookService;
    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     * @throws Exception
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        /** @var Builder $query */
        $query = DriveBook::query();
        $this->filter($query, $request);
        $query->orderBy('date');
        $driveBooks = $query->get();
        return DriveBookResource::collection($driveBooks);
    }

    /**
     * @param DriveBook $driveBook
     * @return DriveBookResource
     */
    public function show(DriveBook $driveBook): DriveBookResource
    {
        return new DriveBookResource($driveBook);
    }


    /**
     * @param DriveBookStoreRequest $request
     * @return DriveBookResource
     */
    public function store(DriveBookStoreRequest $request): DriveBookResource
    {
        /** @var DriveBook $driveBook */
        $driveBook = $this->driveBookService->create($request);
        return new DriveBookResource($driveBook);
    }

    /**
     * @param DriveBookUpdateRequest $request
     * @param DriveBook $driveBook
     * @return DriveBookResource
     */
    public function update(DriveBookUpdateRequest $request, DriveBook $driveBook): DriveBookResource
    {
        if ($driveBook->user_id !== $request->user('api')->id) {
            throw new CannotUpdateDriveException();
        }

        /** @var DriveBook $driveBook */
        $driveBook = $this->driveBookService->update($request, $driveBook);
        return new DriveBookResource($driveBook);
    }

    /**
     * @param DriveBookDestroyRequest $request
     * @param DriveBook $driveBook
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(DriveBookDestroyRequest $request, DriveBook $driveBook): JsonResponse
    {
        if ($driveBook->user_id !== $request->user('api')->id) {
            throw new CannotDeleteDriveException();
        }

        $driveBook->delete();
        return response()->json([
            'id' => $driveBook->id,
            'successfully deleted' => true
        ], 200);
    }

    /**
     * @param Builder $builder
     * @param Request $request
     * @throws Exception
     */
    private function filter(Builder $builder, Request $request): void
    {
        if ($request->has('month') && $request->has('year')) {
            $year = $request->year;
            $month = $request->month;
            $fromAt = Carbon::createFromDate($year, $month, 1)->setTime(0, 0);
            $toAt = Carbon::createFromDate($year, $month, 1)->endOfMonth();
            $builder
                ->where('date', '>=', $fromAt)
                ->where('date', '<=', $toAt);
        }
    }
}
