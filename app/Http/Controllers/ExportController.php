<?php

namespace App\Http\Controllers;

use App\Exports\DriveBookExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ExportController extends Controller
{
    /**
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function __invoke(Request $request): BinaryFileResponse
    {
        return Excel::download(new DriveBookExport, 'drive-book.csv');
    }
}
