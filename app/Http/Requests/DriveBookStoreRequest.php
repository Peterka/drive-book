<?php
declare(strict_types=1);

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DriveBookStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @var User|null $user */
        $user = request()->user('api');

        if (is_null($user)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', Rule::in(['private', 'business'])],
            'date'	=> 'required|date|date_format:Y-m-d',
            'place_from' => 'required|string',
            'place_to' => 'required|string',
            'through' => 'nullable|string',
            'back' => 'boolean',
            'purpose_drive' => 'nullable|string',
            'tach_before' => 'nullable|integer',
            'tach_after' => 'nullable|integer',
        ];
    }
}
