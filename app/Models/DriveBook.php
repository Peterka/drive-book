<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class DriveBook extends Model
{
    use SoftDeletes;

    /** @var string  */
    protected $table = 'drive_books';

    /** @var array  */
    protected $guarded = ['id'];

    /** @var array  */
    protected $casts = [
        'date' => 'datetime',
        'back' => 'boolean',
    ];

    /** @var array  */
    protected $fillable = [
        'place_from', 'place_to', 'through', 'purpose_drive', 'tach_before', 'tach_after',
    ];

    /** @var array  */
    protected $attributes = [
        'type' => 'private',
        'back' => false,
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
