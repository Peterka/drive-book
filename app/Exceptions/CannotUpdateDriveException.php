<?php
declare(strict_types=1);

namespace App\Exceptions;

use LogicException;
use Throwable;

class CannotUpdateDriveException extends LogicException
{
    /**
     * CannotUpdateBuildingException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $message = 'CANNOT_UPDATE_DRIVE',
        int $code = 99701,
        ?Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }

    public function getHttpCode(): int
    {
        return 403;
    }

    /**
     * @return string
     */
    public function getUserMessage(): string
    {
        /** @var string $message */
        $message = 'This user cannot update this drive.';
        return $message;
    }
}
