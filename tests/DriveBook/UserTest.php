<?php

namespace Tests\DriveBook;

use App\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\TestResponse;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class UserTest extends TestCase
{
    /** @var Collection  */
    private $users;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var Collection $users */
        $users = factory(User::class, 10)->create();
        $this->users = $users;
    }

    public function testUserList()
    {
        /** @var User $causer */
        $causer = $this->users->first();
        $this->be($causer, 'api');
        /** @var TestResponse $response */
        $response = $this->get('api/users');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($this->users->count(), count($response->json('data')));
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                ]
            ]
        ]);
    }

    public function testUserShow(?bool $requsted = true)
    {
        /** @var User $causer */
        $causer = $this->users->first();
        $this->be($causer, 'api');
        if ($requsted) {
            /** @var User $user */
            $user = $causer;
        } else {
            /** @var User $user */
            $user = $this->users->last();
        }
        /** @var TestResponse $response */
        $response = $this->get('api/users/' . $user->id);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($user->id, $response->json('data.id'));
        $this->assertSame($user->name, $response->json('data.name'));
        $this->assertSame($user->email, $response->json('data.email'));
        $this->assertSame($user->email_verified_at->format('c'), $response->json('data.email_verified_at'));
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'email',
                'email_verified_at',
            ]
        ]);
    }


    public function testRequestUser()
    {
        $requested = true;
        $this->testUserShow($requested);
    }

    public function testAuthorization(?bool $authorization = false, ?bool $expires = true)
    {
        if ($authorization && !$expires) {
            /** @var TestResponse $response */
            $response = $this->get('api/users');
            $this->assertSame(200, $response->getStatusCode());
        } else {
            $this->withoutExceptionHandling([
                AuthenticationException::class
            ]);

            /** @var TestResponse $response */
            $response = $this->json('GET', 'api/users');
            $this->assertSame(401, $response->getStatusCode());
        }
    }

    public function testLoginUser()
    {
        $this->testAuthorization();

        $user = factory(User::class)->create([
           'name' => 'Petr Peterka',
           'email' => 'peterkapetr888@gmail.com',
        ]);

        // Login with bad password
        $response = $this->post('api/login', [
            'email' => $user->email,
	        'password' => '123456788',
        ]);
        $this->assertSame(401, $response->getStatusCode());

        //Login with right password
        $response = $this->post('api/login', [
            'email' => $user->email,
            'password' => '123456789',
        ]);

        $minutes = config('jwt.ttl');
        $this->assertSame('Bearer Token', $response->json('token_type'));
        $this->assertSame(now()->addMinutes($minutes)->format('c'), $response->json('expires_in'));
        $this->testAuthorization(true, false);

        //Hour before the token expires
        Carbon::setTestNow(now()->addMinutes($minutes-60));
        $this->testAuthorization(true, false);


        //Hour after the token expires
        $this->expectException(TokenExpiredException::class);
        Carbon::setTestNow(now()->addMinutes(120));
        auth('api')->logout();
        $this->testAuthorization(true, true);
    }
}
