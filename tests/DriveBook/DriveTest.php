<?php

namespace Tests\DriveBook;

use App\Models\DriveBook;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\TestCase;

class DriveTest extends TestCase
{
    /** @var User */
    private $user;

    /** @var Collection  */
    private $drives;

    protected function setUp(): void
    {
        parent::setUp();
        /** @var Collection $drives */
        $drives = factory(DriveBook::class, 10)->create();
        $this->drives = $drives;
        /** @var User $user */
        $user = factory(User::class)->create();
        $this->user = $user;
    }

    public function testDriveList()
    {
        $this->be($this->user, 'api');
        /** @var TestResponse $response */
        $response = $this->get('api/drive-book');
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($this->drives->count(), count($response->json('data')));
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'type',
                    'date',
                    'through',
                    'back',
                    'purpose_drive',
                    'tach_before',
                    'tach_after',
                    'user_id',
                ]
            ]
        ]);
    }

    public function testDriveShow()
    {
        $this->be($this->user, 'api');
        /** @var DriveBook $drive */
        $drive = $this->drives->first();
        /** @var TestResponse $response */
        $response = $this->get('api/drive-book/' . $drive->id);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertSame($drive->id, $response->json('data.id'));
        $this->assertSame($drive->type, $response->json('data.type'));
        $this->assertSame($drive->date->format('c'), $response->json('data.date'));
        $this->assertSame($drive->place_from, $response->json('data.place_from'));
        $this->assertSame($drive->user_id, $response->json('data.user_id'));
        $response->assertJsonStructure([
            'data' => [
                'id',
                'type',
                'date',
                'place_from',
                'place_to',
                'through',
                'back',
                'purpose_drive',
                'tach_before',
                'tach_after',
                'user_id',
            ]
        ]);
    }

    public function testDriveStore()
    {
        $this->be($this->user, 'api');
        /** @var TestResponse $response */
        $response = $this->post('api/drive-book', [
            'type' => 'private',
            'date' => '2019-11-03',
            'place_from' => 'Sokolov',
            'place_to' => 'Plzeň',
            'through' => 'Karlovy Vary',
            'back' => false,
            'purpose_drive' => 'Work drive',
            'tach_before' => 350,
            'tach_after' => 250,
        ]);
        $this->assertSame(201, $response->getStatusCode());
        $this->assertSame('private', $response->json('data.type'));
        $this->assertSame('Sokolov', $response->json('data.place_from'));
        $this->assertSame('Work drive', $response->json('data.purpose_drive'));
        $this->assertSame($this->user->id, $response->json('data.user_id'));
        $response->assertJsonStructure([
            'data' => [
                'id',
                'type',
                'date',
                'place_from',
                'place_to',
                'through',
                'back',
                'purpose_drive',
                'tach_before',
                'tach_after',
                'user_id',
            ]
        ]);
    }

    public function testDriveUpdate()
    {
        $this->be($this->user, 'api');
        /** @var TestResponse $storeResponse */
        $storeResponse = $this->post('api/drive-book', [
            'type' => 'private',
            'date' => '2019-11-03',
            'place_from' => 'Sokolov',
            'place_to' => 'Plzeň',
            'through' => 'Horni Slavkov',
            'back' => true,
            'purpose_drive' => 'Work drive',
            'tach_before' => 350,
            'tach_after' => 150,
        ]);
        $this->assertSame(201, $storeResponse->getStatusCode());

        /** @var TestResponse $updateResponse */
        $updateResponse = $this->patch('api/drive-book/' . $storeResponse->json('data.id'), [
            'type' => 'business',
            'date' => '2019-11-04',
            'place_from' => 'Karlovy Vary',
            'place_to' => 'Praha',
            'through' => 'Lubenec',
            'back' => false,
            'purpose_drive' => 'Work drive',
            'tach_before' => 350,
            'tach_after' => 190,
        ]);
        $this->assertSame(200, $updateResponse->getStatusCode());
        $this->assertSame('business', $updateResponse->json('data.type'));
        $this->assertSame('Karlovy Vary', $updateResponse->json('data.place_from'));
        $this->assertSame('Work drive', $updateResponse->json('data.purpose_drive'));
        $this->assertSame($this->user->id, $updateResponse->json('data.user_id'));
        $updateResponse->assertJsonStructure([
            'data' => [
                'id',
                'type',
                'date',
                'place_from',
                'place_to',
                'through',
                'back',
                'purpose_drive',
                'tach_before',
                'tach_after',
                'user_id',
            ]
        ]);
    }

    public function testDriveDestroy()
    {
        /** @var DriveBook $drive */
        $drive = $this->drives->first();
        /** @var User $owner */
        $owner = User::query()->where('id', $drive->user_id)->first();
        $this->be($owner, 'api');
        /** @var TestResponse $response */
        $response = $this->delete('api/drive-book/' . $drive->id);
        $this->assertSame(200, $response->getStatusCode());
        $this->assertTrue($response->json('successfully deleted'));
        $response->assertJsonStructure([
            'id',
            'successfully deleted'
        ]);
    }

    public function testAuthorization()
    {
        $this->withoutExceptionHandling([
            AuthenticationException::class
        ]);

        /** @var TestResponse $responseList */
        $responseList = $this->get('api/drive-book');
        $this->assertSame(302, $responseList->getStatusCode());

        /** @var TestResponse $responseStore */
        $responseStore = $this->json('POST', 'api/drive-book', [
            'type' => 'private',
            'date' => '2019-11-03',
            'place_from' => 'Sokolov',
            'place_to' => 'Plzeň',
            'through' => 'Karlovy Vary',
            'back' => false,
            'purpose_drive' => 'Work drive',
            'tach_before' => 250,
            'tach_after' => 250,
        ]);
        $this->assertSame(401, $responseStore->getStatusCode());
    }
}
