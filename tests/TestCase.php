<?php

namespace Tests;

use Illuminate\Support\Facades\Schema;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();

        Schema::defaultStringLength(191);
        $this->artisan('migrate:fresh');
        $this->withFactories(__DIR__ . '/../database/factories');
        $this->withoutExceptionHandling();
    }
}
