# CHANGELOG

## 6.2.2 (2019-12-27)
### Fixed:
-  export, factory DriveBook

## 6.2.1 (2019-12-27)
### Fixed:
- parameters into `drive-books` table

## 6.2.0 (2019-12-23)
### Feature:
- export `drive-books` to xlsx, csv

## 6.1.1 (2019-12-15)
### Added:
- new parameters into `drive-books` table

### Fixed:
- filtering drives by date

## 6.1.0 (2019-11-03)
### Feature:
- create API (CRUD operation) for DriveBook

## 6.0.0 (2019-11-03)
### Added:
- First version
