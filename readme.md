# DriveBook

- run localy server `php artisan serve`
- run docker-compose `docker-compose up -d`
- run docker for testing `docker run -d --tmpfs /var/lib/postgre:rw --rm -e POSTGRES_PASSWORD=secret -e POSTGRES_USER=testing -e POSTGRES_DB=testing -p 54321:5432 postgres:11`
