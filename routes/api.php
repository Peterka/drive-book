<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DriveBookController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [LoginController::class, 'login'])->name('login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('drive-book/export', ExportController::class)->name('export');
    Route::resource('drive-book', DriveBookController::class)->only(['index', 'show', 'store', 'update', 'destroy']);
    Route::resource('users', UserController::class)->only(['index', 'show']);
    Route::get('user', function (Request $request) {
        $causer = $request->user('api');
        return redirect()->route('users.show', [$causer]);
    });
});
